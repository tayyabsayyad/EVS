natural=tree should be used for individual trees, and not for (inter alia) groups of trees, tree stumps, shrubs, or places where a tree can be planted (tree pits): all of the latter have occurred when external sources of tree data have been imported.

More usually the issue is one of the sheer number of trees which could be mapped. Therefore people tend to restrict themselves to specific targets:

    Isolated trees in public parks & gardens

    Street trees

    Notable trees: large, unusual, named or memorials. These may be in cities, but also in the countryside and in woods and forests (for instance: The Major Oak, the Nottingham Inclosure Oak, the Jagiellonian Oak).

    Trees in campus areas of universities, schools etc

Avenues of trees can be mapped as natural=tree_row, as individual trees (with denotation=avenue), or both (see recent discussion on tagging).
Tree data on OSM can be used for many purposes, from just making the standard Carto-CSS rendering look more detailed, to providing lots of information of interest to enthusiastic dendrophiles.
A couple of words of caution:

    Mapping trees from aerial images is surprisingly error prone. Where I've checked places mapped this way there are often mistakes, and the locations are often rather inaccurate when compared with official data when available. Most usually trees get missed because the canopies of trees which are quite distinct on the ground merge when seen from above. Using a GPS to locate trees is not a whole lot better either.

    Trees just mapped as natural=tree are much less useful than those with the type of tree, ideally to species, but if not to genus or family. At a minimum try and add leaf_type and leaf_cycle. Diameter or circumference is very useful (and much more useful than height which will change regularly), but slows down surveying.

    Official tree datasets are very valuable, but should really be checked as trees get chopped down, new trees planted. In addition not all identifications are accurate, and some include plants which are not trees but bushes.

Not on OSM, but collected from survey & open data sources, the site of the Bristol Tree Forum is an excellent example of what is possible for a single city.
level 1
joeybab3
6 points ·
1 year ago

This area is a redwood forest so like there’s lots of trees and I often see people adding hundreds of tree points for forests as well as areas like this for the landscaping. What is the proper use for the “tree” points?
level 2
vutuan202
7 points ·
1 year ago

I believe some tree points are needed, such as those that are close to a particular amenities (entrance, footpaths or BBQ stoves...). So if tha tree is shown on the map as a point/node, people can find the amenity very easily.
level 1
EncapsulatedPickle
4 points ·
1 year ago

Most single trees are just on landuse=grass between other features like foot paths, roads, buildings, etc. These are worth mapping because they are relatively large and distinct compared to their surroundings (as opposed to mapping trees in a forest or private properties).



[22/05 7:08 pm] Milind Oka: ज्याची गरज ते नाही
फक्त जे सोपे ते
[22/05 7:09 pm] Milind Oka: Chess risbood.
College principal.
[22/05 7:09 pm] Milind Oka: बॅचलर ऑफ education
[22/05 7:12 pm] Milind Oka: Not understood education
Not understood sports importance
Haven't understood life
[26/05 11:24 am] Milind Oka: Bread Labour

Bread for 30 years.
Labour afterwards
[26/05 12:59 pm] Milind Oka: Composting is a service to nation as well as service to nature.

plantation like projects can be initiated by public only. they require no cost. the small amount needed can be happily 
borned by individuals or NGOs

ref Kagal nursery epaoer sakal

If we had not put plastic or any synthetic material in dumping ground we would have huge amount of ready valuable manure at the dumping ground.
